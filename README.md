# PROLAMP WIKI #

This WIKI would normally document whatever steps are necessary to get your plugin up and running.

### How do I get set up? ###

* Download the plugin from https://www.spigotmc.org/resources/prolamp.86712/
* Upload it to your spigot server plugins folder
* Restart server

### API ###

* ProLampAPI.addLamp(Lamp lamp)
* ProLampAPI.removeLamp(Lamp lamp)
* ProLampAPI.createLamp(Location location, String command, Player owner) throws Throwable
* ProLampAPI.getLamp(Block block)